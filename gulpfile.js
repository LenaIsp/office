var 
	gulp = require('gulp'),
	browserSync = require('browser-sync'),
	pug = require('gulp-pug'),
	sass = require('gulp-sass'),
	rename = require('gulp-rename'),
	autoprefixer = require('gulp-autoprefixer'),
	plumber = require('gulp-plumber');

var 
	folder = 'umachallenge';

gulp.task('serve', function() { 
	browserSync({
		server: {
			baseDir: "./src",
			directory: true,
			ignored: "./src/not"
		},
		notify: false

	});
});

gulp.task('pug', function() {
	gulp.src(['!src/not/**/*', './src/**/*.pug'])
	.pipe(plumber())
	.pipe(pug({ pretty: true }))
	.pipe(gulp.dest('./src'))
	.pipe(browserSync.stream());
});


gulp.task('sass', function () {
	return gulp.src(['!src/not/**/*', './src/**/css/main.scss']) 
		.pipe(plumber())
		.pipe(sass())
		.pipe(autoprefixer({
            overrideBrowserslist:  ['last 2 versions'],
            cascade: false
        }))
		.pipe(gulp.dest('./src'))
		.pipe(browserSync.stream());
});

gulp.task('default', ['serve', 'sass', 'pug'], function () {
	gulp.watch(['!src/not/**/*', './src/**/css/**/*.scss'], ['sass']); 
	gulp.watch(['!src/not/**/*', './src/**/*.pug'], ['pug']);
});