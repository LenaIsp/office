/*$.ajax({
	method: "POST",
	data: "name=prozharka",
	url: "https://tnt4.ru/servicescripts/program_episodes_list"
}).done(output)
.fail(function() {
	console.log("Error!!");
});*/

/*$(document).on('click', '.folder__item', function (e) {
	e.preventDefault();
	var dataName = $(this).attr('data-name');
	var imgSrc = `bin/office/images/folder/${dataName}_img.png`
    $('.video__wrapper img').attr('src', imgSrc);
    $('.video__wrapper').addClass(dataName);
	// добавление видео
	name = $(this).attr('class').slice(16);
	$('.video__wrapper')
	.append('<div class="video__cont video__cont--' + name + '"><a href="https://tnt4.ru/" target="_blank"><img src="bin/office/images/folder/' + name + '_img.png" alt=""></a><a href="https://tnt4.ru/" target="_blank"><img src="bin/office/images/folder/' + name + '_doc.png" alt=""></a></div>');
	$('.video').css({'opacity': '1','visibility':'visible'});
});*/

$( document ).ready(function() {

	$(document).on('click', '.folder__item', function (e) {
		e.preventDefault();
		name = $(this).attr('data-name')
		$('.video__wrapper')
		.append('<div class="video__cont video__cont--' + name + '"><a href="https://tnt4.ru/" target="_blank"><img src="bin/office/images/folder/' + name + '_img.png" alt=""></a><a href="https://tnt4.ru/" target="_blank"><img src="bin/office/images/folder/' + name + '_doc.png" alt=""></a></div>');
		$('.video').css({'opacity': '1','visibility':'visible'});
	});

	$(document).on('click', '.video__cont img', function (e) {
		e.preventDefault();
		$('.video__wrapper')
		.append('<div class="video__cont-person video__cont-person--' + name + '"><div class="video__close js-close"></div><div class="video__cont-text"><h3>Майкл Скотт</h3><p>Босс регионального офиса. Майкл может перечислить всю свою зарплату на благотворительность ради того, чтобы впечатлить сотрудников, а потом тайно пытаться вернуть деньги назад. Может устроить весёлый розыгрыш и при этом довести человека до слёз. Но за экспрессивным поведением напоказ скрывается добрый и чувствительный человек.За роль этого запоминающегося персонажа актёр Стив Карелл получил «Золотой глобус» за лучшую мужскую роль в комедии.</p></div></div>');
		$('.video').css({'opacity': '1','visibility':'visible'});
	});


	$(document).on('click', '.js-play', function (e) {
		// создание видео
		e.preventDefault();
		$('.video__wrapper').addClass('video__wrapper--full').append('<iframe src="https://www.youtube.com/embed/C9jECCI7xnU" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" class="video__frame"></iframe>');
		$('.video__close').addClass('video__close--full');
		$('.video').css({'opacity': '1','visibility':'visible'});
	});

	$(document).on('click', '.js-close', function () {
		// удаление видео
		$('.video').css({'opacity': '0','visibility':'hidden'});
		$('.video__wrapper iframe, .video__cont, .video__cont-person').remove();
		$('.video__wrapper').removeClass('video__wrapper--full');
		$('.video__close').removeClass('video__close--full');
	});
});