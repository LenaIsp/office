/*$.ajax({
	method: "POST",
	data: "name=prozharka",
	url: "https://tnt4.ru/servicescripts/program_episodes_list"
}).done(output)
.fail(function() {
	console.log("Error!!");
});*/

$( document ).ready(function() {
	
	$(document).on('click', '.folder__item', function (e) {
		e.preventDefault();
		// добавление видео
		var name = $(this).attr('class').slice(16);
		$('.video__wrapper')
		.append('<div class="video__cont video__cont--' + name + '"><a href="https://tnt4.ru/" target="_blank"><img src="bin/office/images/folder/' + name + '_img.png" alt=""></a><a href="https://tnt4.ru/" target="_blank"><img src="bin/office/images/folder/' + name + '_doc.png" alt=""></a></div>');
		$('.video').css({'opacity': '1','visibility':'visible'});
	});

	$(document).on('click', '.js-play', function (e) {
		// создание видео
		e.preventDefault();
		$('.video__wrapper').addClass('video__wrapper--full').append('<iframe src="https://www.youtube.com/embed/C9jECCI7xnU" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" class="video__frame"></iframe>');
		$('.video__close').addClass('video__close--full');
		$('.video').css({'opacity': '1','visibility':'visible'});
	});

	$(document).on('click', '.js-close', function () {
		// удаление видео
		$('.video').css({'opacity': '0','visibility':'hidden'});
		$('.video__wrapper iframe, .video__cont').remove();
		$('.video__wrapper').removeClass('video__wrapper--full');
		$('.video__close').removeClass('video__close--full');
	});
});