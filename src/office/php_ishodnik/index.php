<?php
switch ($RenderMode) {
    case RENDER_MODE_DEFAULT:
        ?><!DOCTYPE html>
<html lang="ru" data-livestyle-extension="available">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/icon" href="images/favicon.ico">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>Американская прожарка</title>
    <link rel="stylesheet" href="bin/amerstandup/css/main.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!--Google-->
    <script type="text/javascript" title="Universal Analytics">
      (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
          (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
          m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
      })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
      ga('create', 'UA-66343339-4', 'auto');
      ga('send', 'pageview');
    </script>
    <!--Yandex.Metrika counter-->
    <script type="text/javascript">
      (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
          try {
            w.yaCounter37539305 = new Ya.Metrika({
              id: 37539305,
              clickmap: true,
              trackLinks: true,
              accurateTrackBounce: true,
              webvisor: true
            });
          } catch (e) {
          }
        });
        var n = d.getElementsByTagName("script")[0],
          s = d.createElement("script"),
          f = function () {
            n.parentNode.insertBefore(s, n);
          };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
      
        if (w.opera == "[object Opera]") {
          d.addEventListener("DOMContentLoaded", f, false);
        } else {
          f();
        }
      })(document, window, "yandex_metrika_callbacks");
    </script>
  </head>
  <body>
    <section class="section">
      <div class="top">
        <div class="top__banner"><img class="desctop" src="bin/amerstandup/images/banner.png" alt=""><img class="mobile" src="bin/amerstandup/images/banner_mob.png" alt=""></div>
        <div class="top__logo"><a href="http://tnt-online.ru/"><img src="bin/amerstandup/images/logo_tnt4.png" alt=""></a></div>
      </div>
    </section>
    <section class="section">
      <div class="content">
        <p class="typografy typografy--text">Звёзды американского<br class="mobile"> стендапа на&nbsp;ТНТ4!</p>
        <p class="typografy typografy--text">Лучшие выступления американских комиков, которые сделали из&nbsp;них звёзд мирового масштаба. Эми Шумер, Энтони Джесельник, Джефф Росс, Крис&nbsp;Д&rsquo;Елия и&nbsp;Бо&nbsp;Бёрнем стали популярны в&nbsp;начале XXI&nbsp;века, каждый со&nbsp;своим уникальным голосом и&nbsp;неповторимым стилем. Теперь на&nbsp;них ориентируются, их&nbsp;цитируют, над ними смеются миллионы во&nbsp;всем мире. Лучший зарубежный стендап в&nbsp;России&nbsp;&mdash; только на&nbsp;телеканале ТНТ4!</p>
      </div>
    </section>
    <section class="section">
      <div class="play js-play">
        <div class="play__pic"><img src="bin/amerstandup/images/play.png" alt=""></div>
        <div class="play__text"><span>смотреть&nbsp;промо</span></div>
      </div>
      <div class="banner">
        <div class="banner__pic"><img src="bin/amerstandup/images/comik.png" alt=""></div>
      </div>
    </section>
    <section class="section">
      <div class="comik">
        <div class="comik__item comik__item--dgeff">
          <div class="comik__image"><img class="desctop" src="bin/amerstandup/images/comik1_desc.png" alt=""><img class="mobile" src="bin/amerstandup/images/comik1_mob.png" alt=""></div>
          <div class="comik__text"><span class="typografy typografy--textsmall">Мастер<br class="mobile"> &laquo;Прожарок&raquo; Джефф&nbsp;Росс может жёстко шутить не&nbsp;только над голливудскими<br class="mobile"> звёздами.</span></div>
          <div class="comik__play">
            <div class="play js-play0">
              <div class="play__pic"><img src="bin/amerstandup/images/play.png" alt=""></div>
              <div class="play__text"><span>смотреть</span></div>
            </div>
          </div>
        </div>
        <div class="comik__item comik__item--cris">
          <div class="comik__image"><img class="desctop" src="bin/amerstandup/images/comik2_desc.png" alt=""><img class="mobile" src="bin/amerstandup/images/comik2_mob.png" alt=""></div>
          <div class="comik__text"><span class="typografy typografy--textsmall">Молодой американец из&nbsp;успешной семьи Крис&nbsp;Д&rsquo;Елия находит абсурд в&nbsp;повседневной жизни и&nbsp;новостных сводках.</span></div>
          <div class="comik__play comik__play--cris">
            <div class="play js-play1">
              <div class="play__pic"><img src="bin/amerstandup/images/play.png" alt=""></div>
              <div class="play__text"><span>смотреть</span></div>
            </div>
          </div>
        </div>
      </div>
      <div class="comik">
        <div class="comik__item comik__item--ent">
          <div class="comik__image"><img class="desctop" src="bin/amerstandup/images/comik3_desc.png" alt=""><img class="mobile" src="bin/amerstandup/images/comik3_mob.png" alt=""></div>
          <div class="comik__text"><span class="typografy typografy--textsmall">Вызывающе неполиткорректный бунтарь Энтони Джесельник разрушает многие табу в&nbsp;юморе.</span></div>
          <div class="comik__play comik__play--ent">
            <div class="play js-play2">
              <div class="play__pic"><img src="bin/amerstandup/images/play.png" alt=""></div>
              <div class="play__text"><span>смотреть</span></div>
            </div>
          </div>
        </div>
        <div class="comik__item comik__item--bo">
          <div class="comik__image"><img class="desctop" src="bin/amerstandup/images/comik4_desc.png" alt=""><img class="mobile" src="bin/amerstandup/images/comik4_mob.png" alt=""></div>
          <div class="comik__text"><span class="typografy typografy--textsmall">Музыкальный вундеркинд Бо&nbsp;Бёрнем&nbsp;&mdash; первый&nbsp;комик, раскрутившийся благодаря интернету.</span></div>
          <div class="comik__play comik__play--bo">
            <div class="play js-play3">
              <div class="play__pic"><img src="bin/amerstandup/images/play.png" alt=""></div>
              <div class="play__text"><span>смотреть</span></div>
            </div>
          </div>
        </div>
        <div class="comik__item comik__item--emm">
          <div class="comik__image comik__image--emm"><img class="desctop" src="bin/amerstandup/images/comik5_desc.png" alt=""><img class="mobile" src="bin/amerstandup/images/comik5_mob.png" alt=""></div>
          <div class="comik__text"><span class="typografy typografy--textsmall">Одна из&nbsp;самых знаменитых девушек-комиков Эми Шумер рассуждает о&nbsp;гендерном неравенстве и взаимоотношениях полов.</span></div>
          <div class="comik__play comik__play--emm">
            <div class="play js-play4">
              <div class="play__pic"><img src="bin/amerstandup/images/play.png" alt=""></div>
              <div class="play__text"><span>смотреть</span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section">
      <div class="footer">
        <div class="icons-soc">
          <div class="icons-hover"><a class="vk round-soc" href="https://vk.com/tnt4" target="_blank"><span><i class="fab fa-vk fa-2x"></i></span></a><a class="ok round-soc" href="https://ok.ru/tnt4" target="_blank"><span><i class="fab fa-odnoklassniki fa-2x"></i></span></a><a class="fb round-soc" href="https://facebook.com/tnt4ru" target="_blank"><span><i class="fab fa-facebook-f fa-2x"></i></span></a><a class="ins round-soc" href="https://instagram.com/tnt4ru" target="_blank"><span><i class="fab fa-instagram fa-2x"></i></span></a><a class="youtube round-soc" href="https://youtube.com/tnt4ru" target="_blank"><span><i class="fab fa-youtube fa-2x"></i></span></a><a class="rutube-soc" href="https://rutube.ru/video/person/1131656/" target="_blank"><img class="rutube-logo fa-2x" src="bin/amerstandup/images/rutube-logo.png" alt=""></a></div>
        </div>
      </div>
    </section>
    <div class="video">
      <div class="video__wrapper">
        <div class="video__close js-close"></div>
      </div>
    </div>
    <!--script-->
    <script src="bin/amerstandup/js/jquery.min.js"></script>
    <script src="bin/amerstandup/js/main.js"></script>
    <!-- /script-->
    <noscript>
      <div> <img src="https://mc.yandex.ru/watch/37539305" style="position:absolute; left:-9999px;" alt=""></div>
    </noscript>
    <!--tns-counter.ru-->
    <script type="text/javascript">(new Image()).src = '//www.tns-counter.ru/V13a***R>' + document.referrer.replace(/\*/g, '%2a') + '*tnt_ru/ru/UTF-8/tmsec=tnt4_total/' + Math.round(Math.random() * 1E9);</script>
    <noscript><img src="https://www.tns-counter.ru/V13a****tnt_ru/ru/UTF-8/tmsec=tnt4_total/" width="1" height="1" alt=""></noscript>
  </body>
</html><?php
        Content::Finalize();
        break;
}
?>